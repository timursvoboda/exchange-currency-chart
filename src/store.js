import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    avApiOptions: {
      // Alpha Vantage API Options
      mainUrl: "https://www.alphavantage.co/query",
      fromCurrency: "AED",
      toCurrency: "USD",
      chartFunction: "FX_INTRADAY",
      realtimeFuncton: "CURRENCY_EXCHANGE_RATE",
      outputSize: "compact",
      chartInterval: 1,
      apikey: "41FJUND9GYBDZOD6"
    },
    exchangeRate: "-",
    updateExchangeRateInterval: 15000,
    chartData: [],
    chartOptions: {
      colors: ["#41b883"],
      hAxis: {
        gridlines: { count: 3 }
      },
      vAxis: {
        format: "###.#####"
      }
    },
    loading: false,
    error: false
  },
  mutations: {
    updateExchangeRate(state, payload) {
      state.exchangeRate = payload;
    },
    updateChart(state, payload) {
      state.chartData = payload;
    },
    addRowToChartData(state, payload) {
      state.chartData.splice(1, 0, payload);
    },
    setLoading(state, payload) {
      state.loading = payload;
    }
  },
  actions: {
    updateExchangeRate({ commit, state }) {
      commit("setLoading", true);
      axios
        .get(
          `${state.avApiOptions.mainUrl}?function=${state.avApiOptions.realtimeFuncton}&from_currency=${state.avApiOptions.fromCurrency}&to_currency=${state.avApiOptions.toCurrency}&apikey=${state.avApiOptions.apikey}`
        )
        .then(response => {
          const date = new Date(
            response.data["Realtime Currency Exchange Rate"][
              "6. Last Refreshed"
            ]
          );
          const rate = +response.data["Realtime Currency Exchange Rate"][
            "5. Exchange Rate"
          ];

          commit("addRowToChartData", [date, { v: rate, f: rate }]);
          commit("updateExchangeRate", rate);
          commit("setLoading", false);
        })
        .catch(() => {
          state.error = true;
        });
    },

    initExchangeRate({ commit, state }) {
      commit("setLoading", true);
      axios
        .get(
          `${state.avApiOptions.mainUrl}?function=${state.avApiOptions.chartFunction}&from_symbol=${state.avApiOptions.fromCurrency}&to_symbol=${state.avApiOptions.toCurrency}&interval=${state.avApiOptions.chartInterval}min&outputsize=${state.avApiOptions.outputSize}&apikey=${state.avApiOptions.apikey}`
        )
        .then(response => {
          const responseDataObject = response.data[`Time Series FX (1min)`];

          const chartData = [["Date", "Rate"]];

          for (let key in responseDataObject) {
            const rate = +responseDataObject[key]["1. open"];

            chartData.push([new Date(key), { v: rate, f: rate }]);
          }

          commit("updateExchangeRate", chartData[chartData.length - 1][1].f);
          commit("updateChart", chartData);
          commit("setLoading", false);
        })
        .catch(() => {
          state.error = true;
        });
    }
  },
  getters: {
    getExchangeRate(state) {
      return state.exchangeRate;
    },
    getChartData(state) {
      return state.chartData;
    },
    getChartOptions(state) {
      return state.chartOptions;
    },
    getUpdateExchangeRateInterval(state) {
      return state.updateExchangeRateInterval;
    },
    getLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    }
  }
});
