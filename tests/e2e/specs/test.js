// https://docs.cypress.io/api/introduction/api.html

describe("ExchangeRate component", () => {
  it("renders exchange rate", () => {
    cy.server();
    cy.visit("/");

    cy.route({
      url: "https://www.alphavantage.co/**"
    }).as("route_new-exchange-rate");

    cy.wait("@route_new-exchange-rate", { timeout: 30000 }).then(xhr => {
      const exchangeRate =
        xhr.response.body["Realtime Currency Exchange Rate"][
          "5. Exchange Rate"
        ];

      cy.get(".exchange-rate-cell").should($div => {
        expect(+$div.text()).to.be.equal(+exchangeRate);
      });
    });
  });
});
